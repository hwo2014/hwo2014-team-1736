
/**
 * Example:
			var speedChart = this.chart.draw({
				data : [ 
					{ color:'blue',values:[[1,0],[2,3],[3,6],[4,5],[5,5],[6,12],[7,3],[8,4],[9,2]],
						highlights : [ {name:'lap0',value:[2.3]}, {name:'lap1',value:[7.6]} ] },
					{ color:'green',values:[[1,0],[4,6],[7,4],[8,6],[9,1]],
						highlights : [] }
				],
				x : 0,
				y : 0,
				width : 400,
				height : 300,
				xTitle : "ticks",
				yTitle : "speed"
			});
 */
function DebugChart(paper) {

	this.paper = paper;
	
}
DebugChart.prototype = {

	draw : function(args) {
		var data = args.data;
		var width = args.width;
		var height = args.height;
		
		var resolutionX = 10;
		var resolutionY = 10;
		var min = new this.paper.Point(100,100);
		var max = new this.paper.Point(0,0);
		var size = new this.paper.Size(0,0);
		var group = new this.paper.Group();
		
		// find data boundaries
		for(var n=0; n<data.length; n++) {
			var values = data[n].values;
			for(var i=0; i<values.length; i++) {
				max.x = Math.max(max.x,values[i][0]);
				max.y = Math.max(max.y,values[i][1]);
				min.x = Math.min(min.x,values[i][0]);
				min.y = Math.min(min.y,values[i][1]);
			}
		}
		size.width = max.x - min.x;
		size.height = max.y - min.y;
		// normalize data
		var ratioView = width/height;
		var ratioData = size.width/size.height;
		// too high
		if( ratioView/ratioData < 0.95 ) {
			resolutionY *= ratioData;
		} 
		// too wide
		else if( ratioView/ratioData > 1.05 ) {
			resolutionX /= ratioData;
		}
		size.width *= resolutionX;
		size.height *= resolutionY;
		min.x *= resolutionX;
		min.y *= resolutionY;
		max.x *= resolutionX;
		max.y *= resolutionY;
		var fontSize = size.width/50;
		for(var n=0; n<data.length; n++) {
			var values = data[n].values;
			for(var i=0; i<values.length; i++) {
				values[i][0] *= resolutionX;
				values[i][1] *= resolutionY;
			}
		}
		// draw X coordinates
		var xCoords = new this.paper.Path.Line({
			from : [ 0, size.height+min.y ],
			to : [ size.width, size.height+min.y ],
			strokeColor : 'black' 
		});
		group.addChild(xCoords);
		// draw X ticks
		var nTicks = 10.0;
		var xStep = size.width/nTicks;
		for(var i=1; i<=nTicks; i++) {
			var value = i*xStep;
			var tickPoint = new this.paper.Point( [ value, size.height ] );
			var tick = new this.paper.Path.Line({
				from : tickPoint,
				to : [ tickPoint.x, 0 ],
				strokeColor : 'lightgrey' 
			});
			var tickText = new this.paper.PointText({
				point: tickPoint.add( [ 0, fontSize ] ),
				content: round( (min.x+value)/resolutionX ),
				justification: 'center',
				fontSize : fontSize
			});
			group.addChild(tick);
			group.addChild(tickText);
		}
		// draw X title
		var xTitle = new this.paper.PointText({
			point: new this.paper.Point( [ size.width-fontSize*3, size.height+fontSize*3 ] ),
			content: args.xTitle,
			justification: 'center',
			fontSize : fontSize*2.0
		});
		group.addChild(xTitle);
		
		// draw Y coordinates
		var yCoords = new this.paper.Path.Line({
			from : [ 0, size.height ],
			to : [ 0, 0 ],
			strokeColor : 'black' 
		});
		group.addChild(yCoords);
		// draw Y ticks
		var nTicks = 10.0;
		var yStep = size.height/nTicks;
		for(var i=1; i<=nTicks; i++) {
			var value = i*yStep;
			var tickPoint = new this.paper.Point( [ 0, size.height-value ] );
			var tick = new this.paper.Path.Line({
				from : tickPoint,
				to : [ size.width, tickPoint.y ],
				strokeColor : 'lightgrey' 
			});
			var tickText = new this.paper.PointText({
				point: tickPoint.add( [ -fontSize, 0 ] ),
				content: round( (min.y+value)/resolutionY ),
				justification: 'center',
				fontSize : fontSize
			});
			group.addChild(tick);
			group.addChild(tickText);
		}
		// draw Y title
		var yTitle = new this.paper.PointText({
			point: new this.paper.Point( [ -fontSize*3, size.height/2 ] ),
			content: args.yTitle,
			justification: 'center',
			fontSize : fontSize*2.0
		});
		yTitle.rotate(-90);
		group.addChild(yTitle);

		// draw highlights for each curve
		for(var n=0; n<data.length; n++) {
			var highlights = data[n].highlights;
			for(var i=0; i<highlights.length; i++) {
				var highlightPoint = new this.paper.Point( [highlights[i].value*resolutionX,0] );
				var highlight = new this.paper.Path.Line({
					from : highlightPoint,
					to : [highlightPoint.x,size.height],
					strokeColor : data[n].color,
					dashArray : [10, 4]
				});
				var highlightTitle = new this.paper.PointText({
					point: highlightPoint.add( [-fontSize,fontSize] ),
					content: highlights[i].name,
					justification: 'center',
					fontSize : fontSize*1.5,
					fillColor : data[n].color
				});
				highlightTitle.rotate(-90);
				group.addChild(highlight);
				group.addChild(highlightTitle);
			}
		}
		
		// draw each curve
		for(var n=0; n<data.length; n++) {
			var values = data[n].values;
			var path = new this.paper.Path({
				strokeColor : data[n].color,
				strokeWidth: 2,
				strokeJoin: 'round'
			});
			for(var i=0; i<values.length; i++) {
				path.add( [values[i][0]-min.x, size.height-(values[i][1]-min.y)] );
			}
			//path.smooth();
			group.addChild(path);
		}
		
		// scale chart to target window
		var bounds = group.bounds;
		group.scale(width/bounds.width, height/bounds.height);
		// position chart in the middle of the window
		group.position = new this.paper.Point(args.x + width/2, args.y + height/2);
		
		return group;
	}
	
};

function round(n) {
	return Number((n).toFixed(1));
}

module.exports = {
		DebugChart : DebugChart
};
