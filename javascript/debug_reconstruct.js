var paper_lib = require('paper');
var util = require('util');
var fs = require('fs');

var parameters = require('./parameters.js');
var metrics = require('./metrics.js');
var client_track = require('./client_track.js');
var client_car = require('./client_car.js');
var debug_logger = require('./debug_logger.js');
var debug_plotter = require('./debug_plotter.js');
var botName = "Cotrino";

/**************************************************************************************/

function DebugReconstruct(logfile) {
	// create paper
	this.paper = new paper_lib.PaperScope();
	// create canvas
	this.canvas = new this.paper.Canvas(global.parameters.trackWidth, global.parameters.trackHeight);
	// configure Paper.js
	this.paper.setup(this.canvas);
	
	// white background
	new this.paper.Path.Rectangle({
	    point: [0, 0],
	    size: [this.canvas.width, this.canvas.height],
	    strokeColor: 'white',
	    fillColor: 'white'
	});	
	
	this.logger = new debug_logger.DebugLogger();
	this.plotter = new debug_plotter.DebugPlotter(this.paper);
	this.cars = {};
	this.tick = 0;
	this.gameId = null;
	this.crashed = false;
	this.track = new client_track.Track(this.paper);
	
	this._generate(logfile);
}
DebugReconstruct.prototype = {
	
	track : null,
	cars : null,
	logger : null,
	plotter : null,
	paper : null,
	canvas : null,
	gameId : null,
	tick : 0,
	
	_generate : function(logfile) {
		var json = this.logger.get(logfile);
		console.log(json.length+" JSON messages found");

		// - fetch messages, once at a time.
		// - reconstruct race as in the actual race. 
		for( var i=0; i<json.length; i++) {
			var item = json[i];
			
			// catch game identifier
			if( item.type == 'server' && typeof item.message.gameId !== 'undefined' ) {
				this.gameId = item.message.gameId;
			}
			
			// read game information
			if( item.type == 'server' && item.message.msgType == 'gameInit' ) {
				// read track properties
				this.track.decode(item.message.data.race.track);
				
				/**
				 * read race properties
				 * - race.laps
				 * - race.maxLapTimeMs
				 * - race.quickRace
				 */
				this.race = item.message.data.race.raceSession;

				// read car properties
				for(var n=0; n<item.message.data.race.cars.length; n++) {
					var id = item.message.data.race.cars[n].id.name;
					this.cars[id] = new client_car.Car(
							this.paper, 
							item.message.data.race.cars[n],
							this.track,
							this.race,
							true
							);
				}
			}
			
			// read car positions at every tick
			if( item.type == 'server' && item.message.msgType == 'carPositions'
				&& this.crashed == false ) {
				this.getTick(item.message);
				// read car positions
				for(var n=0; n<item.message.data.length; n++) {
					var id = item.message.data[n].id.name;
					this.cars[id].set(this.tick, item.message.data[n]);
				}
				
				/*
				var throttle = this.cars[botName].getThrottle(this.tick);
				var switchLane = this.cars[botName].getSwitch(this.tick);
				if( switchLane != "no" ) {
					console.log(this.tick+": "+throttle+" "+switchLane);
				}
				*/
			}
			
			// read car crashes
			if( item.type == 'server' && item.message.msgType == 'crash' ) {
				this.getTick(item.message);
				var id = item.message.data.name;
				this.cars[id].setCrash(this.tick);
				this.crashed = true;
			}
			
			// read car respawn
			if( item.type == 'server' && item.message.msgType == 'spawn' ) {
				this.getTick(item.message);
				this.crashed = false;
			}
			
			// read car steering
			if( item.type == 'client' && item.message.msgType == 'throttle' ) {
				this.getTick(item.message);
				this.cars[botName].setThrottle(this.tick, item.message.data, item.responseTime/1000);
			}

		}

		// plot summary chart
		this.plotter.generateCharts ({
			track : this.track, 
			cars : this.cars, 
			ticks : this.tick, 
			laps : this.race.laps, 
			canvas : this.canvas, 
			gameId : this.gameId 
		});
		
		// clone all pending activities
		for(var key in this.cars) {
			this.cars[key].destroy();
		}
		
	},
	
	// read ticks
	getTick : function(data) {
		if( data.gameTick != null ) {
			this.tick = data.gameTick;
		}
	},
};

/**************************************************************************************/

global.parameters = new parameters.Parameters();
global.metrics = new metrics.Metrics();

var path = './debug/';
if( global.parameters.modeLearn ) {
	path = path + 'data/';
}
var files = fs.readdirSync(path);
files.sort(function(a, b) {
    return fs.statSync(path + b).mtime.getTime() - 
           fs.statSync(path + a).mtime.getTime();
});
var done = false;
for(var i=0; i<files.length && !done; i++) {
	if( files[i].indexOf(".log") != -1 ) {
		console.log(files[i]);
		var debug = new DebugReconstruct(path+files[i]);
		if( global.parameters.modeSimulation ) {
			done = true;
		}
	}
}
