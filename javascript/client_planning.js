"use strict";

var util = require('util');

var helper = require('./debug_helper.js');
var physics_lib = require('./client_physics.js');

function RouteStep(id, piece, track) {
	this.id = id;
	this.change = "no";
	this.piece = piece;
	this.lane = 0;
	this.maxSpeed = 10.0;
	this.throttle = 1.0;
	this.diffAngle = 0;
	this.totalLength = 0;
	this.updated = false;
	/*
	this.track = track;
	this.curvature = 0;
	this.deltaCurvature = 0;
	this.piecesAhead = Math.round(global.parameters.piecesAhead / 2);
	this.diffAngle = 0;
	this.totalLength = 0;
	*/
}
RouteStep.prototype = {
	clone : function(id) {
		var c = new RouteStep();
		for(var key in this) {
			c[key] = this[key];
		}
		c[id] = id;
		return c;
	}	
};

function Planning(physics, track, race) {

	this.physics = physics;
	this.track = track;
	this.race = race;
	
	this.segments = null;
	
	this.maxCurvature = 0;
	this.minCurvature = 0;
	this.maxDeltaCurvature = 0;
	this.minDeltaCurvature = 0;
	
	this.route = [];
	for( var n = 0; n < this.track.pieces.length; n++ ) {
		this.route[n] = new RouteStep(n, this.track.getPiece(n), this.track);
	}
	
	this.predictions = [];
	
}
Planning.prototype = {

	// calculate route
	calculate : function(status, tick) {
		
		// calculate segments, if not yet done
		if( this.segments == null ) {
			this.calculateSegments();
		}
		
		// clone previous route steps for following laps
		var step = this.get(status.coveredPieces);
		
		// set lanes
		step.lane = status.laneStartIndex;
		this.get(step.id+1).lane = status.laneEndIndex;
		
		// decide whether to switch to another lane in the next piece
		this.chooseLane(step);
		
		/**
		 * Maximize speed for future tick to maximize car angle without crashing.
		 */
		
		var currentStatus = status.clone();
		currentStatus.throttle = global.parameters.maxThrottle;
		var minAcceleration = global.parameters.maxAcceleration;
		var minSpeed = global.parameters.maxSpeed;
		var nextStatus = null;
		
		// look for maximum acceleration for differentes forecast distances
		for( var factor=global.parameters.ticksForecastFactor; factor >= 1; factor-=2 ) {
			
			var nextTick = Math.ceil( status.speed  * factor );
			if( nextTick == 0 ) {
				nextTick = 2;
			}
			//console.log("Tick "+currentStatus.tick+" looking at "+nextTick+" (speed="+currentStatus.speed+")");
			
			// 1) calculate max speed for curved pieces, and thus required acceleration
			currentStatus.acceleration = global.parameters.maxAcceleration;
			nextStatus = this.calculateAcceleration(currentStatus, nextTick);
			/*console.log("\t[Tick "+currentStatus.tick+" => "+nextStatus.tick+"]"
					+" piece "+currentStatus.piece.id+" => "+nextStatus.piece.id
					+" max speed = "+currentStatus.speed);*/
			
			minAcceleration = Math.min( currentStatus.acceleration, minAcceleration );
			minSpeed = Math.min( nextStatus.speed, minSpeed );
		}
		
		currentStatus.acceleration = minAcceleration;
		
		// 2) fine-tune max speed backwards to make it reachable via throttling
		this.calculateThrottle(currentStatus, currentStatus.tick+1);
		/*
		console.log("\t[Tick "+currentStatus.tick+" => "+nextStatus.tick+"] "
				+"piece "+currentStatus.piece.id+" => "+nextStatus.piece.id
				+", speed "+round(currentStatus.speed)+" => "+round(minSpeed)
				+", acc "+round(currentStatus.acceleration)+" => "+round(minAcceleration)
				+", throttle="+round(currentStatus.throttle));
		*/
		
		// TODO do all calculations taking into account current lane!
				
		/*
		if( tick > 100 && tick < 150 ) {
		console.log("Tick ["+currentStatus.tick+" .. "+nextStatus.tick+"] "
				+status.piece.id+"=>"
				+nextStatus.piece.id
				+", speed ["+round(currentStatus.speed)+" .. "+round(nextStatus.speed)+"]"
				+", acc="+round(nextStatus.acceleration)
				+" => throttle="+round(currentStatus.throttle));
		}	
		*/
		
		if( tick > 326 ) {
		//	process.exit();
		}
		
		// return requested throttle
		return currentStatus.throttle;
	},
	
	/**
	 * Calculate max speed at next step which maximizes car angle without crashing.
	 * - Current speed is fixed.
	 * - Predicted angle is set to the maximum.
	 * - We can play with acceleration to reach max speed at next step. 
	 */
	calculateAcceleration : function(status, ticks) {

		var pred = null;
		
		// get maximum car angle before crashing
		var targetAngleError = global.parameters.carAngleError;
		var targetAngle = global.parameters.carMaxAngle;
		var targetAngleMax = targetAngle * (1.0+targetAngleError);
		var targetAngleMin = targetAngle * (1.0-targetAngleError);
		
		// straight piece
		if( status.piece.curvedPiece == false ) {
			pred = this.physics.predict(status, ticks, false);
			status.acceleration = global.parameters.maxAcceleration;
			pred.speed = global.parameters.maxSpeed;
		}
		// curved piece
		else {
			var i = 0;
			var done = false;
			while( !done && i < global.parameters.planningIterations ) {
				
				// predict the angle after given ticks
				pred = this.physics.predict(status, ticks, false);
				
				if( Math.abs(pred.angle) > targetAngleMax
						&& status.acceleration > global.parameters.minAcceleration ) {
					status.acceleration -= global.parameters.accelerationChangeStep;
				}
				else if( Math.abs(pred.angle) < targetAngleMin
						&& status.acceleration < global.parameters.maxAcceleration ) {
					status.acceleration += global.parameters.accelerationChangeStep;
				} else {
					done = true;
				}
				/*
				console.log("piece="+status.piece.id+".."+pred.piece.id
						+", tick="+status.tick+".."+pred.tick
						+", angle="+round(status.angle)+".."+round(pred.angle)
						+", speed="+round(status.speed)+".."+round(pred.speed)
						+", acc="+round(status.acceleration)+".."+round(pred.acceleration));
				*/
				
				i++;
			}
			
			/*
			if( Math.abs(pred.angle) > targetAngle ) {
				console.log("ERROR impossible piece "+status.piece.id+"!"
						+"\nSTATUS: "+util.inspect(status,{depth:0})
						+"\nPRED: "+util.inspect(pred,{depth:0}));
				//status.speed = lastSpeed;
				process.exit();
			}
			*/
		}
		
		return pred;
	},
	
	/**
	 * Calculate max throttle for the desired acceleration.
	 */
	calculateThrottle : function(status, ticks) {

		var i = 0;
		var done = false;
		var targetAccelerationError = global.parameters.carAngleError;
		var targetAccelerationMax = status.acceleration * (1.0+targetAccelerationError);
		var targetAccelerationMin = status.acceleration * (1.0-targetAccelerationError);
		status.throttle = global.parameters.maxThrottle;
		
		// loop changing this step's throttle
		while( !done && i < global.parameters.planningIterations ) {
			
			// predict the acceleration after given ticks
			var pred = this.physics.predict(status, ticks, false);
			
			if( Math.abs(pred.acceleration) > targetAccelerationMax
					&& status.throttle > global.parameters.minThrottle ) {
				status.throttle *= global.parameters.throttleChangeFactor;
			}
			else if( Math.abs(pred.acceleration) < targetAccelerationMin
					&& status.throttle < global.parameters.maxThrottle ) {
				status.throttle /= global.parameters.throttleChangeFactor;
			} else {
				done = true;
			}
			/*
			console.log("Target: "+round(status.acceleration)
					+", throttle="+round(status.throttle)
					+", pred="+round(pred.acceleration));
			*/
			i++;
		}
		
		/*
		// unreachable, let's hope we can fix it in the previous step
		if( pred.speed > expected.speed ) {
			console.log("["+status.tick+"] ERROR impossible speed for piece "+status.piece.id+"!");
			status.throttle = 0.0;
		}*/
	},
	
	// clone previous route steps for following laps
	get : function(n) {
		if( this.route[n] == null ) {
			var pieceId = n % this.track.pieces.length;
			this.route[n] = this.route[pieceId].clone(n);
		}
		return this.route[n];
	},
	
	chooseLane : function(step) {
		var nextStep = this.get(step.id+1);
		var nextPiece = this.track.getPiece(step.id+1);
		
		if( nextPiece.switchPiece && step.change != "changed" ) {
			var segment = this.segmentMapping[nextPiece.id];
			// TODO this has the problem that we may only be able to change 1 line per switch
			// that is, we should only change if the preferred one is just 1 step ahead
			var preferredLane = getMin(segment.lanesLength, step.lane);
			if( step.lane > preferredLane ) {
				step.change = "Left";
			} else if( step.lane < preferredLane ) {
				step.change = "Right";
			} else {
				step.change  ="no";
			}
			//console.log(piece.id+": "+step.lane+" => "+preferredLane+" ["+segment.lanesLength+"]");
			nextStep.lane = preferredLane;
		}
	},
	
	/**
	 * Find the best path:
	 * - Minimum distance
	 * - Maximum speed
	 * 
	 * A segment:
	 * - is a collection of tracks pieces between switches.
	 * - E.g. { length: [300, 320...], pieces:[piece1,piece2,piece3] }
	 * - is separated from others by switches.
	 */
	calculateSegments : function() {
		var s = 0;
		var segments = [];
		var segmentMapping = [];
		var lanes = this.track.lanes.length;
		// TODO una posible putada es que haya alguna pieza de un solo carril
		for( var n=0; n<this.track.pieces.length; n++ ) {
			var piece = this.track.pieces[n];
			if( segments[s] == null ) {
				segments[s] = { lanesLength: [], pieces: [] };
				for(var lane=0; lane<lanes; lane++) {
					segments[s].lanesLength[lane] = 0;
				}
			}

			if( piece.switchPiece ) {
				s++;
				segments[s] = { lanesLength: [], pieces: [] };
				for(var lane=0; lane<lanes; lane++) {
					segments[s].lanesLength[lane] = 0;
				}
			} else {
				segments[s].pieces.push(n);
				// accumulate the length per lane 
				for(var lane=0; lane<lanes; lane++) {
					segments[s].lanesLength[lane] += piece.lanes[lane].length;
				}
				// TODO calculate segment accumulated angle
			}

			segmentMapping[n] = segments[s];
		}
		//console.log(util.inspect(segments));
		this.segments = segments;
		this.segmentMapping = segmentMapping;
	}
	
};

function round(n) {
	return Number((n).toFixed(1));
}

function getMin(arr, current) {
	var min = arr[current];
	var minIndex = current;
	
	for (var i = 0; i < arr.length; i++) {
	    if (arr[i] < min && i != current) {
	    	minIndex = i;
	    	min = arr[i];
	    }
	}
	return minIndex;
}

module.exports = {
		Planning : Planning
};