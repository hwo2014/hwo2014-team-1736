// official libraries
var net = require('net');
var JSONStream = require('JSONStream');
var util = require('util');
var paper_lib = require('paper');

// own modules
var parameters = require('./parameters.js');
var metrics = require('./metrics.js');
var client_track = require('./client_track.js');
var client_car = require('./client_car.js');
var debug_logger = require('./debug_logger.js');
var debug_plotter = require('./debug_plotter.js');

// read arguments
var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];
console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

//global parameters & metrics
global.parameters = new parameters.Parameters();
global.metrics = new metrics.Metrics();

if( !global.parameters.modeRace ) {
	console.log("Race mode not enabled!");
	process.exit();
}

/**************************************************************************************/

function Game() {
	// create paper
	this.paper = new paper_lib.PaperScope();
	// create canvas
	this.canvas = new this.paper.Canvas(global.parameters.trackWidth, global.parameters.trackHeight);
	// configure Paper.js
	this.paper.setup(this.canvas);
	
	// white background
	new this.paper.Path.Rectangle({
	    point: [0, 0],
	    size: [this.canvas.width, this.canvas.height],
	    strokeColor: 'white',
	    fillColor: 'white'
	});	
	
	// DEBUG: create logger & plotter
	if (global.parameters.debug) {
		this.logger = new debug_logger.DebugLogger();
		this.plotter = new debug_plotter.DebugPlotter(this.paper);
	}
	this.cars = {};
	this.track = new client_track.Track(this.paper);
	this.time = process.hrtime();
	this.ongoing = true;
	this.crashed = false;
	this.othertrack = null;
	//this.othertrack = "germany";
	//this.othertrack = "usa";
}
Game.prototype = {
	
	track : null,
	cars : null,
	logger : null,
	plotter : null,
	paper : null,
	canvas : null,
	gameId : null,
	tick : 0,
	time : 0,
	ongoing : true,
	othertrack : null,
	
	run : function() {
		
		var game = this;
		
		// PROTOCOL: 1. Bot sends join
		this.client = net.connect(serverPort, serverHost, function() {
			// simple join
			var msg = {
					msgType : "join",
					data : {
						name : botName,
						key : botKey
					}
				};
			// create race in specific track
			if( game.othertrack != null ) {
				msg = {
						msgType : "createRace", 
						data : {
							botId: {
								name : botName,
								key : botKey
							},
							trackName: game.othertrack,
							password: "dontcare",
							carCount: 1
						}};
			}
			return game.send(msg);
		});

		// receive message from server
		jsonStream = game.client.pipe(JSONStream.parse());
		jsonStream.on('data', function(data) {

			// timestamp at which data was received
			game.time = process.hrtime();
			
			// DEBUG: log message
			if (global.parameters.debug) {
				//console.log("[Server] "+util.inspect(data));
				game.logger.add({
					type : 'server',
					message : data
				});
			}
			switch (data.msgType) {
			
			// PROTOCOL: 1b. Server confirms join
			case 'join':
				console.log('Joined');
				game.ping();
				break;
			// PROTOCOL: 2. Server sends yourCar
			case 'yourCar':
				console.log('Car '+data.data.name+' accepted');
				game.ping();
				break;
			// PROTOCOL: 3. Server sends gameInit
			case 'gameInit':
				console.log('Race details');
				game.ping();
				
				// catch game identifier
				if( typeof data.gameId !== 'undefined' ) {
					game.gameId = data.gameId;
				}
				
				// read track properties
				game.track.decode(data.data.race.track);
				
				/**
				 * read race properties
				 * - race.laps
				 * - race.maxLapTimeMs
				 * - race.quickRace
				 */
				game.race = data.data.race.raceSession;

				// read car properties
				for(var n=0; n<data.data.race.cars.length; n++) {
					var id = data.data.race.cars[n].id.name;
					if( id == botName ) {
						game.cars[id] = new client_car.Car(
								game.paper, 
								data.data.race.cars[n],
								game.track,
								game.race,
								false	// no debug!
								);
					}
				}
				break;
			// PROTOCOL: 4. Server sends gameStart
			case 'gameStart':
				console.log('Race started');
				game.getTick(data);
				game.ping();
				break;
			// PROTOCOL: 5. Server sends carPositions
			case 'carPositions':
				game.getTick(data);
				
				if( game.crashed == false ) {
					// read car positions
					for(var n=0; n<data.data.length; n++) {
						var id = data.data[n].id.name;
						game.cars[id].set(game.tick, data.data[n]);
					}
					// PROTOCOL: Switching lanes
					// FIXME switch lane should only be sent once per tick, without other messages!
					var switchLane = game.cars[botName].getSwitch(game.tick);
					if( switchLane != "no" && switchLane != "changed" ) {
						game.send({
							msgType : "switchLane",
							data : switchLane,
							gameTick: game.tick
						});
					} else {
						// PROTOCOL: 6. Bot sends throttle
						game.send({
							msgType : "throttle",
							data : game.cars[botName].getThrottle(game.tick),
							gameTick: game.tick
						});
					}
					// inform about the car
					if (global.parameters.debug) {
						console.log(game.cars[botName].getDebugInfo(game.tick));
					}
				} else {
					game.ping();
				}
		        break;
		     // PROTOCOL: 8. Server sends gameEnd message
			case 'gameEnd':
				console.log('Race ended');
				game.ping();
				// DEBUG: create visualizations
				if (global.parameters.debug) {
					// save JSON file stream
					game.logger.save(game.track.id);
				}
				break;
			// PROTOCOL: crash
			case 'crash':
				game.getTick(data);
				var id = data.data.name;
				game.cars[id].setCrash(game.tick);
				console.log( game.tick+": crash" );
				/*
				if (global.parameters.debug) {
					// save JSON file stream
					game.logger.save();
				}
				game.ongoing = false;
				game.client.destroy();
				*/
				game.crashed = true;
				break;
			case 'spawn':
				game.crashed = false;
				break;
			case 'turboAvailable':
				// TODO implement turbo handling 
				// {"msgType": "turboAvailable", "data": {  "turboDurationTicks": 30,"turboFactor": 3.0	}}
				game.getTick(data);
				console.log( game.tick+": turbo!" );
				/*
				game.send({
					msgType : "turbo",
					data : "ahi vamos!",
					gameTick: game.tick
				});
				*/
				break;
			case 'dnf':
				game.getTick(data);
				console.log( game.tick+": disqualified. Reason:"+data.data.reason );
				break;
			}
		});

		jsonStream.on('error', function() {
			return console.log("disconnected");
		});
		
	},
	
	// read ticks
	getTick : function(data) {
		if( data.gameTick != null ) {
			this.tick = data.gameTick;
		}
	},

	send : function(json) {
		// DEBUG: log message
		if (global.parameters.debug) {
			//console.log("[Client] "+util.inspect(json));
			// timestamp at which petition was completed
			var diff = process.hrtime(this.time);
			var microseconds = Math.round((diff[0] * 1e9 + diff[1])/1000);
			//console.log('Client answer took %d microseconds', microseconds);
			this.logger.add({
				type : 'client',
				responseTime : microseconds,
				message : json
			});
		}
		if( this.ongoing ) {
			this.client.write(JSON.stringify(json));
			this.client.write('\n');
			//console.log( game.tick+": "+util.inspect(json) );
		}
	},
	
	ping : function() {
		this.send({
			msgType : "ping",
			gameTick: this.tick
		});
	}
};

/**************************************************************************************/

var game = new Game();
game.run();

/**
 * 
 * TODO reestructurar cliente para permitir ejecutar varios como child process y competir entre ellos
 *  
 * TODO aprender la acelaración del coche en la primera recta 
 * 
 * TODO aprender la fricción del terreno en la primera curva 
 * 
 * TODO calcular la máxima velocidad para cada curva en cuanto sepa la fricción 
 * 
 * TODO calcular la velocidad y ángulo objetivo de entrada en cada segmento
 * 
 * TODO el ángulo de desviación se puede acumular, aprender cómo compensarlo 
 * 
 * TODO si hay un coche en la misma línea, hay que cambiar
 * 
 * TODO aprender la ruta de los otros coches e intentar derribarlos 
 * 
 * TODO pensar en putadas que pueden añadir
 * 
 */
