var util = require('util');

var helper = require('./debug_helper.js');
var chartlib = require('./debug_chart.js');

function DebugPlotter(paper) {
	
	this.paper = paper;
	
	this.chart = new chartlib.DebugChart(this.paper);

	// gradient for speed/throttle representation
	this.gradient = {
			startColor : new this.paper.Color(0.0, 1.0, 0.0),
			endColor : new this.paper.Color(1.0, 0.0, 0.0)
	};
	this.gradient.diffRed = this.gradient.endColor.red - this.gradient.startColor.red;
	this.gradient.diffGreen = this.gradient.endColor.green - this.gradient.startColor.green;
	this.gradient.diffBlue = this.gradient.endColor.blue - this.gradient.startColor.blue;

}

DebugPlotter.prototype = {

		track : null,
		cars : null,

		/**
		 * Plot a chart for each lap, drawing each car’s route as colored path, 
		 * with a line at each position with angle & speed.
		 */
		generateCharts : function(args) {
			
			this.track = args.track;
			this.cars = args.cars;
			var laps = args.laps;

			var viewSize = this.paper.view.bounds;
			var lastPositionX = 0;

			var trackChartWidth = viewSize.width / 3;
			var trackChartHeight = viewSize.height / laps;
			for(var l=0; l<laps; l++) {
				// draw track chart
				var trackChart = this._drawTrack(l);
				// position track chart
				trackChart.addChild(new this.paper.PointText({
					point: new this.paper.Point( [0,-80] ),
					content: "Lap "+(l+1),
					justification: 'center',
					fontSize : 40,
					fillColor : 'red'
				}));
				var bounds = trackChart.strokeBounds;
				// scale by width
				if( bounds.width > trackChartWidth ) {
					trackChart.scale(trackChartWidth/bounds.width);
				} else {
					trackChart.scale(trackChartHeight/bounds.height);
				}
				bounds = trackChart.strokeBounds;
				trackChart.position.x = lastPositionX + bounds.width/2;
				trackChart.position.y = trackChartHeight*l + bounds.height/2;
			}
			lastPositionX += trackChartWidth;
			
			// metrics to be shown in charts
			var metrics = global.parameters.metrics;
			
			// draw history charts
			var lastChart = null;
			var chartWidth = viewSize.width / 3;
			var chartHeight = viewSize.height / metrics.length;
			for(var i in metrics) {
				var metric = metrics[i];
				var chart = this._drawChart(metric, "history", chartWidth, chartHeight);
				// reposition chart
				chart.position.x = lastPositionX + chart.bounds.width/2;
				if( lastChart != null ) {
					chart.position.y = chartHeight*i + chart.bounds.height/2;
				}
				lastChart = chart;
			}
			lastPositionX += chartWidth;
			
			// draw error charts
			/*
			lastChart = null;
			for(var i in metrics) {
				var metric = metrics[i];
				var chart = this._drawChart(metric, "error", chartWidth, chartHeight);
				// reposition chart
				chart.position.x = lastPositionX + chart.bounds.width/2;
				if( lastChart != null ) {
					chart.position.y = chartHeight*i + chart.bounds.height/2;
				}
				lastChart = chart;
			}
			lastPositionX += chartWidth;
			*/

			// draw learning charts
			var chart = this._drawNeuralNetChart(chartWidth, chartHeight);
			chart.position.x = lastPositionX + chart.bounds.width/2;
			
			// draw everything
			this.paper.view.draw();
			// save to file
			var file = this.track.id + "_" + args.gameId;
			helper.saveCanvas(args.canvas, file);
			console.log("Generated '"+file+".jpg'");
			
		},
		
		// draw neural net chart
		_drawNeuralNetChart : function(width, height) {

			// prepare data
			var data = [];
			
			var input = "speed";
			var output = "drifting";
			var nnData = this.cars["Cotrino"].physics.learnerDrifting.getDrawableData(input, output);
			
			/*
			var input = "throttle";
			var output = "acceleration";
			var nnData = this.cars["Cotrino"].physics.learnerAcceleration.getDrawableData(input, output);
			*/
			
			data[0] = {
					color : "green",
					values : nnData.actual,
					highlights : []
			};
			data[1] = {
					color : "red",
					values : nnData.predicted,
					highlights : []
			};
			
			return this.chart.draw({
				data : data,
				x : 0,
				y : 0,
				width : width,
				height : height,
				xTitle : input,
				yTitle : output
			});
		},
				
		// draw chart
		_drawChart : function(metric, array, width, height) {

			// prepare data
			var data = [];
			var n = 0;
			for(var key in this.cars) {
				var lap = 0;
				var car = this.cars[key];
				var values = [];
				var highlights = [];
				// just for debugging
				var lastTick = car[array].length;
				for(var tick in car[array]) {
					if( tick < lastTick ) {
						var item = car[array][tick];
						if( item != null ) {
							values.push( [tick,item[metric]] );
							if( item.lap != lap ) {
								highlights.push({name:'lap'+item.lap,value:[tick]});
								lap = item.lap;
							}
						}
					}
				}
				for(var tick in car.crashHistory) {
					if( tick < lastTick ) {
						highlights.push({name:'crash',value:[tick]});
					}
				}
				data[n] = {
						color : car.color,
						values : values,
						highlights : highlights
				};
				n++;
			}
			return this.chart.draw({
				data : data,
				x : 0,
				y : 0,
				width : width,
				height : height,
				xTitle : "ticks",
				yTitle : metric
			});
		},
		
		// draw track
		_drawTrack : function(lap) {
			
			var track = this.track;
			var cars = this.cars;
			
			var group = new this.paper.Group();
			
			// track path
			var trackPath = new this.paper.Path({
				segments: [],
				strokeColor: 'black',
				strokeWidth: 50,
				strokeJoin: 'round',
				fullySelected: false
			});
			group.addChild(trackPath);
			
			// starting point
			var startingPoint = new this.paper.Path.Circle({
		    	center: track.pieces[0].point1.clone(),
		    	radius: 4,
		    	fillColor: 'red'
			});
			group.addChild(startingPoint);
			
			var o = new this.paper.Point([0,0]);
			for( var n=0; n<track.pieces.length; n++ ) {
				var piece = track.pieces[n];
				//console.log(piece.point1);
				trackPath.add([ piece.point1.x, piece.point1.y ]);
				
				// draw piece ID
				o.angle = 90+piece.angle1;
				o.length = -40;
				var pieceText = new this.paper.PointText({
					point: piece.point1.add(o),
					content: n,
					justification: 'center',
					fillColor: 'green',
					fontSize : 20
				});
				group.addChild(pieceText);
			}
			// last point = first point
			trackPath.lastSegment.point = trackPath.firstSegment.point;
			trackPath.closePath(true);
			trackPath.smooth();
			
			// create track lanes
			var lanePaths = [];
			for(var l=0; l<track.lanes.length; l++) {
				var lanePath = new this.paper.Path({
					segments: [],
					strokeColor: 'grey',
					strokeWidth: 2,
					strokeJoin: 'round'
				});
				lanePaths.push(lanePath);
				for( var n=0; n<track.pieces.length; n++ ) {
					var lane = track.pieces[n].lanes[l];
					lanePath.add([ lane.point1.x, lane.point1.y ]);
					// if switch piece, connect all lanes together
					if( track.pieces[n].switchPiece ) {
						for( var l2=0; l2<track.lanes.length; l2++) {
							if( l != l2 ) {
								var lane2 = track.pieces[n].lanes[l2];
								var switchLine = new this.paper.Path.Line({
									from: lane.point1,
									to: lane2.point2,
									strokeColor: 'grey',
									strokeWidth: 2,
									strokeJoin: 'round'
								});
								group.addChild(switchLine);
							}
						}
					}
				}
				// last point = first point
				lanePath.lastSegment.point = lanePath.firstSegment.point;
				lanePath.closePath(true);
				lanePath.smooth();
				group.addChild(lanePath);
			}
			
			if( global.parameters.speedLines ) {
				this._drawSpeedLines(cars, lap, lanePaths, group);
			}
			
			if( global.parameters.trackMetric != null ) {
				this._drawMetric(cars, "history", global.parameters.trackMetric, 
						lap, lanePaths, group);
			}
			
			if( global.parameters.trackError != null ) {
				this._drawMetric(cars, "error", global.parameters.trackError, 
						lap, lanePaths, group);
			}
			
			return group;
		},
		
		/**
		 * draw colored metric on the track (0.0=green ... 1.0=red)
		 */
		_drawMetric : function(cars, array, metric, lap, lanePaths, group) {
			for(var key in cars) {
				var car = cars[key];
				var h = car[array];
				// find metric limits for normalization
				var minMetric = 100;
				var maxMetric = 0;
				var n = 0;
				var average = 0;
				for(var j in h) {
					minMetric = Math.min(minMetric, h[j][metric]);
					maxMetric = Math.max(maxMetric, h[j][metric]);
					average += h[j][metric];
					n++;
				}
				console.log(metric+" "+array+": avg="+average/n+", min="+minMetric+", max="+maxMetric);
				// draw normalized metric
				for(var j in h) {
					if( h[j].lap == lap) {
						var pieceId = h[j].pieceIndex;
						var offset = h[j].inPieceDistance;
						var lane = h[j].laneStartIndex;
						/**
						 * normalize metric to [0.0 .. 1.0]:
						 * value = (measurement - minMetric) / (maxMetric - minMetric)
						 */
						var value = (h[j][metric] - minMetric) / (maxMetric - minMetric);
						//var angle = h[j].angle;
						var curve = lanePaths[lane].curves[pieceId];
						if( curve != null ) {
							var p = curve.getPointAt(offset);
							group.addChild(new this.paper.Path.Circle({
						    	center: p,
						    	radius: 10*value,
						    	fillColor: this.getColor(value)
							}));
							/**
							 * draw throttle lines per car
							 * - take the lane curve
							 * - find the point within that curve, given the offset
							 * - draw normal line with throttle as length
							 */
							/*
							var t = curve.getTangentAt(offset);
							t.length = value;
							var o = p.subtract(t);
							var normalLine = new this.paper.Path.Line({
								from: p,
								to: o.rotate(90, p),
								strokeColor: car.color,
								strokeWidth: 2,
								strokeJoin: 'round'
							});
							group.addChild(normalLine);
							*/
						}
					}
				}
			}
		},
		
		/**
		 * draw speed lines per car
		 * - take the lane curve
		 * - find the point within that curve, given the offset
		 */
		_drawSpeedLines : function(cars, lap, lanePaths, group) {
			for(var key in cars) {
				var car = cars[key];
				for(var j=0; j<car.history.length; j++) {
					var h = car.history;
					if( h[j].lap == lap) {
						var pieceId = h[j].pieceIndex;
						var offset = h[j].inPieceDistance;
						var lane = h[j].laneStartIndex;
						var value = h[j].throttle * 200;
						var angle = h[j].angle;
						var curve = lanePaths[lane].curves[pieceId];
						if( curve != null ) {
							var p = curve.getPointAt(offset);
							var t = curve.getTangentAt(offset);
							t.length = value;
							var o = p.subtract(t);
							var tangentLine = new this.paper.Path.Line({
								from: p,
								to: o,
								strokeColor: 'green',
								strokeWidth: 1,
								strokeJoin: 'round'
							});
							var carLine = new this.paper.Path.Line({
								from: p,
								to: o.rotate(angle, p),
								strokeColor: car.color,
								strokeWidth: 2,
								strokeJoin: 'round'
							});
							group.addChild(tangentLine);
							group.addChild(carLine);
						}
					}
				}
			}
		},
		
		getColor : function(percentFade) {
			return new this.paper.Color(
					(this.gradient.diffRed * percentFade) + this.gradient.startColor.red,
					(this.gradient.diffGreen * percentFade) + this.gradient.startColor.green,
					(this.gradient.diffBlue * percentFade) + this.gradient.startColor.blue
					);
		}
};

module.exports = {
		DebugPlotter : DebugPlotter
};
