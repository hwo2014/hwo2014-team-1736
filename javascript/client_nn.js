"use strict";

var brain = require('brain');
var fs = require('fs');
var util = require('util');

// TODO execute net in separate thread
function NeuralNet(name, inputs, outputs) {
	
	this.name = name;
	
	// use a lower learning rate in production
	var learningRate = global.parameters.netLearningRate;
	if( global.parameters.debugLearn ) {
		learningRate = global.parameters.netLearningRateDebug;
	}
	this.net = new brain.NeuralNetwork({
		hiddenLayers: global.parameters.netLayers[this.name],
		learningRate: learningRate
	});
	this.netData = {};
	// training results
	this.netTrain = {error : 1.0, iterations : 0};
	// error threshold to reach
	this.netError = global.parameters.netError[this.name];
	// maximum training iterations
	this.netIterations = global.parameters.netIterations;
	this.inputs = inputs;
	this.outputs = outputs;
	this.parameterLimits = {};
	for(var i in this.inputs) {
		this.parameterLimits[this.inputs[i]] = {min: 0, max: 0.1};
	}
	for(var i in this.outputs) {
		this.parameterLimits[this.outputs[i]] = {min: 0, max: 0.1};
	}
	
	this.file = './learned/'+this.name+".nn";
	// load network, if existing
	if (fs.existsSync(this.file)) {
		var content = fs.readFileSync(this.file,'utf8');
	    var json = JSON.parse(content);
	    this.net.fromJSON( json.net );
	    this.netData = json.data;
	    this.parameterLimits = json.limits;
	    console.log("[NN "+this.name+"] "+this.file+" loaded with "
	    		+this._getDataSize(this.netData)+" data entries");
	}
	
	this.ticksBetweenTrainings = global.parameters.netTicksBetweenTrainings;
	this.tickCounter = this.ticksBetweenTrainings;
	
	this.trainings = 0;
	this.usage = 0;
	this.requests = 0;
	this.errors = 0;
	
}
NeuralNet.prototype = {
	
	/**
	 * Add current status to learned data
	 * @param status
	 */
	add : function(status) {
		
		var inputData = this.normalizeInput(status);
		var outputData = this.normalizeOutput(status);
		for(var i in this.outputs) {
			var key = this.outputs[i];
			outputData[key] = this.normalize(status, key);
		}
		var hash = this._getHash(inputData);
		// if data for existing item existing, average with new data
		if( this.netData[hash] != null ) {
			for(var key in outputData) {
				outputData[key] = ( outputData[key] + this.netData[hash].output[key] ) / 2;
			}
			//console.log(this.name+": "+hash+" => "+util.inspect(this.netData[hash].output)+" / "+util.inspect(outputData));
		}
		this.netData[hash] = { input: inputData, output: outputData };
		
		this.tickCounter++;
		
		if( this.tickCounter > this.ticksBetweenTrainings ) {
			this.retrain();
		}
	},
	
	retrain : function() {
		
		// extract just the entries, not the hashes
		var array = [];
		for(var i in this.netData) {
			array.push(this.netData[i]);
		}
		this.netTrain = this.net.train(
				array, {
					errorThresh: this.netError,
					iterations: this.netIterations,
					//log: true,			// console.log() progress periodically
					//logPeriod: 10		// number of iterations between logging
				});
		if( this.netTrain.error > this.netError ) {
			console.log("[NN "+this.name+"] train error: "+this.netTrain.error);
		}
		this.tickCounter = 0;
		this.trainings++;
	},
	
	/**
	 * Predict acceleration based on learned data.
	 * @param status
	 */
	predict : function(status) {
		
		var outputData = {};
		
		// if neural net is already prepared, give a prediction
		if( this.netTrain.error <= this.netError ) {
			var inputData = this.normalizeInput(status);
			var results = this.net.run(inputData); 
			outputData = this.denormalizeOutput(results);
			/*
			var diff = status.acceleration-outputData.acceleration;
			if( diff > 0 ) {
				console.log("Diff: "+diff);
			}*/
			this.usage ++;
		} else {
			// use previous values if net is not yet reliable
			for(var i in this.outputs) {
				var key = this.outputs[i];
				outputData[key] = status[key];
			}
		}
		this.requests ++;
		
		return outputData;
	},
	
	feedback : function(outputError) {
		for(var i in this.outputs) {
			var key = this.outputs[i];
			var min = this.parameterLimits[key].min;
			var max = this.parameterLimits[key].max;
			var error = Math.abs( outputError[key] / (max-min) );
			if( error > global.parameters.netErrorUsage ) {
				//console.log("[NN "+this.name+"] error too high: "+error);
				this.retrain();
				this.errors++;
			}
		}
	},
	
	normalizeInput : function(status) {
		var inputData = {};
		for(var i in this.inputs) {
			var key = this.inputs[i];
			inputData[key] = this.normalize(status, key);
		}
		return inputData;
	},
	
	normalizeOutput : function(status) {
		var outputData = {};
		for(var i in this.outputs) {
			var key = this.outputs[i];
			outputData[key] = this.normalize(status, key);
		}
		return outputData;
	},
	
	denormalizeOutput : function(result) {
		var outputData = {};
		for(var i in this.outputs) {
			var key = this.outputs[i];
			outputData[key] = this.denormalize(result, key);
		}
		return outputData;
	},
	
	denormalize : function(result, i) {
		return result[i]
			* (this.parameterLimits[i].max - this.parameterLimits[i].min)
			+ this.parameterLimits[i].min;
	},
	
	normalize : function(status, i) {
		var value = status[i]; 
		this.parameterLimits[i].min = Math.min(this.parameterLimits[i].min, value);
		this.parameterLimits[i].max = Math.max(this.parameterLimits[i].max, value);
		return (value - this.parameterLimits[i].min)
				/(this.parameterLimits[i].max - this.parameterLimits[i].min);
	},
	
	destroy : function() {
		console.log("[NN "+this.name+"] predictions:"+this.usage+"/"+this.requests
				+", trainings: "+this.trainings
				+", #data: "+this._getDataSize(this.netData)
				+", #errors: "+this.errors+"/"+this.usage
				+" ("+Number((100*this.errors/this.usage).toFixed(2))+"%)");
		//var run = this.net.toFunction();
		//console.log("Function: "+run.toString());
		if( global.parameters.modeLearn ) {
			console.log("[NN "+this.name+"] saved to file "+this.file);
			var json = {
					net : this.net.toJSON(),
					data : this.netData,
					limits : this.parameterLimits
			};
			var content = JSON.stringify(json);
			fs.writeFileSync(this.file, content, 
					{ encoding : 'utf8', flag : 'w' }, function(err) {
				if (err) {
					console.log(err);
				}
			});
		}
	},
	
	getDrawableData : function(input, output) {
		
		// set initial status
		var status = {};
		for( var i in this.parameterLimits ) {
			status[i] = this.parameterLimits[i].max;
		}
		var normStatus = this.normalizeInput(status);
		
		// fetch data with moving variable input
		var hashKeys = [];
		var actualValuesHash = {};
		var predictedValuesHash = {};
		//console.log( util.inspect(status) );
		for( var i in this.netData ) {
			var d = this.netData[i];
			var validData = true;
			for( var k in d.input ) {
				if( k != input && d.input[k] != normStatus[k] ) {
					validData = false;
				}
			}
			if( validData ) {
				// actual data
				var x = this.denormalize(d.input, input);
				var y = this.denormalize(d.output, output);
				hashKeys.push( x );
				actualValuesHash[x] = y;
				// predicted data
				status[input] = d.input[input];
				y = this.denormalize( this.predict(status), output );
				predictedValuesHash[x] = y;
			}
		}
		
		// sort data by input value
		hashKeys.sort();
		var actualValues = [];
		var predictedValues = [];
		for( var i=0; i<hashKeys.length; i++ ) {
			var x = hashKeys[i]; 
			var actualY = actualValuesHash[x];
			var predictedY = actualValuesHash[x];
			actualValues.push( [ x, actualY ] );
			predictedValues.push( [ x, predictedY ] );
		}
		
		return { actual : actualValues, predicted : predictedValues };
	},
	
	_getHash : function(inputData) {
		var hash = "";
		for(var i in inputData) {
			// TODO check if better to use 2 decimals
			hash += Number((inputData[i]).toFixed(2)) + "_";
		}
		return hash;
	},
	
	_getDataSize : function(data) {
		var size = 0;
	    for (var key in data) {
	        if (data.hasOwnProperty(key)) size++;
	    }
	    return size;
	}
	
};

module.exports = {
		NeuralNet : NeuralNet
};

