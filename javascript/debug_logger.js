var fs = require('fs');
var util = require('util');

function DebugLogger() {
	this.gameId = "example";
}
DebugLogger.prototype = {

	_list : [],
	
	add : function(message) {
		this._list[this._list.length] = message;
		if( message.message.gameId != null ) {
			this.gameId = message.message.gameId;
		}
	},

	get : function(file) {
		var content = fs.readFileSync(file,'utf8');
	    var lines = content.split("\n");
	    for(var i=0; i<lines.length; i++) {
	    	var line = lines[i].toString();
	    	if( line != undefined && line != "" ) {
	    		//console.log(JSON.parse(line));
	    		this.add(JSON.parse(line));
	    	}
	    }
	    return this._list;
	},

	save : function(trackId) {
		var content = "";
		for(var i=0; i<this._list.length; i++) {
			content += JSON.stringify(this._list[i])+"\n";
		}
		fs.writeFile('./debug/'+trackId+"_"+this.gameId+'.log', content, function(err) {
			if (err) {
				console.log(err);
			}
		});
	}
	
};

module.exports = {
		DebugLogger : DebugLogger
};
