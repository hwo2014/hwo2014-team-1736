"use strict";

var util = require('util');

var nn = require('./client_nn.js');

/**
 * Status of the car at a certain tick.
 */
function TickStatus() {
	this.tick = -1;				// current tick
	this.throttle = 0.0;		// current car throttle, controlled by AI
	this.lastTickStatus = null;	// pointer to last tick status
	this.diffTick = 0;			// time difference to previous tick
	this.speed = 0;				// current speed
	this.acceleration = 0;		// current acceleration
	this.angle = 0;				// current angle of the car with the tangent
	this.drifting = 0;			// current angle acceleration of the car with the tangent
	this.angularSpeed = 0;		// current angular speed (degrees per tick)
	this.centripetalForce = 0;	// = [mass] * speed^2 / r
	this.lap = 0;				// current lap
	this.pieceIndex = -1;		// current piece
	this.piece = null;			// pointer to current piece
	this.inPieceDistance = 0;	// current distance covered within the current piece
	this.totalDistance = 0;		// total distance covered until last tick
	this.pieceLength = 0;		// total length of current piece
	this.coveredAngle = 0;		// total accumulated angle
	this.coveredPieces = 0;		// total completed pieces as absolute value (track pieces * laps)
	this.laneStartIndex = 0;	// current lane at the start of the piece
	this.laneStart = 0;			// pointer to lane at the start of the piece
	this.laneEndIndex = 0;		// current lane at the end of the piece
	this.laneEnd = 0;			// pointer to lane at the end of the piece
	this.carLength = 0;			// car dimensions
	this.carWidth = 0;
	this.carFlag = 0;
	this.carFlagRatio = 0;		// flag distance / car length
	this.pieceAngle = 0;		// angle of the current track piece
	this.pieceRadius = 0;		// radius of the current track piece
	this.crash = false;			// crash at this tick
}
TickStatus.prototype = {
	clone : function() {
		var c = new TickStatus();
		for(var key in this) {
			c[key] = this[key];
		}
		return c;
	}
};

/**
 * Physics engine to predict car movement.
 */
function Physics(paper, track, race, dimensions) {
	this._paper = paper;
	this.track = track;
	this.race = race;
	this.prediction = null;
	this.error = null;
	this.carDimensions = dimensions;

	// learn drifting, based on current speed, track angle & car length
	this.learnerDrifting = new nn.NeuralNet(
			"drifting",
			[
			 	//"centripetalForce",
			 	//"angularSpeed",
				"speed", 
				"pieceAngle",
				"pieceRadius",
				//"carLength",
				//"carFlag",
				"carFlagRatio",
				//"acceleration",	// if you brake, car drifts
				//"throttle",
				//"angle", 		// car angle is an absolute value and spoils the net
				//"drifting"
			],
			[ "drifting" ]
			);

	// learn acceleration, based on current speed & throttle
	this.learnerAcceleration = new nn.NeuralNet(
			"acceleration",
			[
				"throttle", 
				"speed", 
				//"pieceAngle",		// theoretically, acceleration should be lower on curved pieces 
				//"acceleration"	// better without own feedback :)
			],
			[ "acceleration" ]
			);
}
Physics.prototype = {
	
	_paper : null,
	track : null,
	race : null,
	
	/**
	 * learn current car position
	 * @param tick current time
	 * @data car information, as provided by server
	 * @last last TickStatus
	 */
	learn : function(_tick, _data, last) {
		
		var status = new TickStatus();
		var _carData = _data.piecePosition;
		
		if( last == null ) {
			last = new TickStatus();
			last.tick = status.tick-1;
		}
		
		// direct information
		status.tick = _tick;
		status.lastTickStatus = last;
		status.lap = _carData.lap;
		status.laneStartIndex = _carData.lane.startLaneIndex;
		status.laneEndIndex = _carData.lane.endLaneIndex;
		status.carWidth = this.carDimensions.width;
		status.carLength = this.carDimensions.length;
		status.carFlag = this.carDimensions.guideFlagPosition;
		status.carFlagRatio = status.carFlag / status.carLength;
		status.pieceIndex = _carData.pieceIndex;
		status.inPieceDistance = _carData.inPieceDistance;
		status.diffTick = status.tick - last.tick;
		status.coveredPieces = last.coveredPieces;
		// drifting angle must be positive to prevent confusing NN
		//status.angle = Math.abs(_data.angle);
		status.angle = _data.angle;
		status.throttle = last.throttle;
		
		// piece-related information
		this._assignPieceProperties(status, last);
		
		if( status.diffTick == 0 ) {
			console.log("This should not have happended!");
			return;
		}
		
		// calculate car angle acceleration (i.e. how fast the car is drifting) 
		status.drifting = status.angle - last.angle;
		if( Math.abs(status.drifting) > 20 ) {
			status.drifting = last.drifting;
		}
		
		// calculate angular speed: traversed arc divided by time 
		status.coveredAngle = last.coveredAngle;
		var angularLength = this._getAngularLength(status, last);
		status.coveredAngle += angularLength;
		status.angularSpeed = angularLength / status.diffTick;
		
		// calculate linear speed
		status.speed = ( status.totalDistance - last.totalDistance ) / status.diffTick;
		// 1) if not precise enough, just assume acceleration hasn't changed.
		//status.speed = last.speed + last.acceleration;
		// if speed is an outlier, discard
		if( status.speed > 10 && status.speed > last.speed*2 ) {
			status.speed = last.speed;
		}
		
		// calculate linear acceleration
		status.acceleration = status.speed - last.speed;
		// discard extreme accelerations (e.g. due to crash)
		if( Math.abs(status.acceleration) > 5
				&& Math.abs(status.acceleration) > Math.abs(last.acceleration*2) ) {
			status.acceleration = 0;
		}
		
		// calculate pseudo centripetal force
		if( status.piece.curvedPiece ) {
			status.centripetalForce = Math.pow( status.speed, 2 ) / status.piece.radius;
		} else {
			status.centripetalForce = 0;
		}
		
		//console.log(util.inspect(status, {depth:0}));

		// pass this data to the drifting learner
		this.learnerDrifting.add(status);
		
		// pass this data to the acceleration learner 
		// only if not simulating; otherwise, throttle is wrong.
		if( global.parameters.modeSimulation == false ) {
			this.learnerAcceleration.add(status);
		}
		
		return status;
	},
	
	/**
	 * predict next position, based on current parameters & throttle
	 * @param tick
	 * @param status
	 */
	predict : function(status, diffTick, useForErrorCalculation) {
		
		var pred = status.clone();
		
		pred.diffTick = diffTick; 
		pred.tick = status.tick + pred.diffTick;
		
		// calculate acceleration & drifting using neural network
		pred.acceleration = this.learnerAcceleration.predict(status).acceleration;
		
		// calculate speed based on predicted acceleration
		pred.speed = status.speed + pred.acceleration * pred.diffTick;
				
		// calculate distance based on speed
		pred.totalDistance = status.totalDistance + pred.speed * pred.diffTick;
		pred.inPieceDistance = status.inPieceDistance + pred.speed * pred.diffTick;

		// if moving to next piece, assign all piece properties
		// FIXME this will not work when moving more than 1 piece
		var _pieceLength = status.pieceLength;
		while( pred.inPieceDistance > _pieceLength ) {
			var _inPieceDistance = pred.inPieceDistance - _pieceLength;
			pred.inPieceDistance = _pieceLength;
			
			var newPred = pred.clone();
			newPred.pieceIndex = (pred.pieceIndex+1) % this.track.pieces.length;
			newPred.inPieceDistance = _inPieceDistance;
			this._assignPieceProperties(newPred, pred);
			_pieceLength = newPred.pieceLength;

			pred = newPred;
			//console.log("[@"+pred.tick+"]: jump from "+status.pieceIndex+" to "+pred.pieceIndex);
		}
		
		// calculate car angle acceleration (drifting) using neural network
		pred.drifting = this.learnerDrifting.predict(status).drifting;

		// calculate car angle
		pred.angle += pred.drifting * pred.diffTick;
		
		// calculate angular speed
		var angularLength = this._getAngularLength(pred, status);
		pred.coveredAngle += angularLength;
		pred.angularSpeed = angularLength / pred.diffTick;

		// calculate pseudo centripetal force
		if( pred.piece.curvedPiece ) {
			pred.centripetalForce = Math.pow( pred.speed, 2 ) / pred.piece.radius;
		} else {
			pred.centripetalForce = 0;
		}
		
		if( useForErrorCalculation ) {
			this.prediction = pred;
		}
		return pred;
	},
	
	_assignPieceProperties : function(status, last) {
		status.piece = this.track.pieces[status.pieceIndex];
		status.laneStart = status.piece.lanes[status.laneStartIndex];
		status.laneEnd = status.piece.lanes[status.laneEndIndex];
		status.pieceAngle = 0;
		status.pieceRadius = 0;
		if( status.piece.curvedPiece ) {
			// TODO calculate new piece angle if changing lanes
			status.pieceAngle = Math.abs(status.piece.angle);
			status.pieceRadius = status.piece.radius;
		}
		status.pieceLength = last.pieceLength;
		
		// calculate total covered distance, including all laps
		status.totalDistance = last.totalDistance;
		// a) if piece has changed...
		if( status.pieceIndex != last.pieceIndex ) {

			// 1) count another piece
			status.coveredPieces++;
			
			// 2) add the last portion of the piece
			status.totalDistance += last.pieceLength - last.inPieceDistance;
			
			// 3) add the current portion in the current piece
			status.totalDistance += status.inPieceDistance;

			// 3) calculate length of new piece
			status.pieceLength = status.piece.getLength(status.laneStartIndex, status.laneEndIndex);
		}
		// b) still same piece
		else {
			status.totalDistance += (status.inPieceDistance - last.inPieceDistance);
			/*
			if( status.laneStartIndex != status.laneEndIndex ) {
				console.log("Changing lane: i="+status.inPieceDistance+" / t="+status.pieceLength+" / s="+status.laneStart.length);
			}
			*/
		}
	},
	
	feedback : function(pred, status) {
		var error = status.clone();	
		error.acceleration -= pred.acceleration;
		error.speed -= pred.speed;
		error.totalDistance -= pred.totalDistance;
		error.drifting -= pred.drifting;
		error.angle -= pred.angle;
		error.angularSpeed -= pred.angularSpeed; 
		error.coveredAngle -= pred.coveredAngle;
		//console.log(round(error.angle));
		
		// force learner to retrain if error too high
		if( status.crash == false ) {
			/* FIXME this must be enabled during race to adapt to new settings
			if( global.parameters.modeSimulation == false ) {
				this.learnerAcceleration.feedback(error);
			}
			this.learnerDrifting.feedback(error);
			*/
		}
		
		this.error = error;
		return error;
	},
	
	destroy : function() {
		this.learnerAcceleration.destroy();
		this.learnerDrifting.destroy();
	},
	
	_getAngularLength : function(status, last) {
		var angularLength = 0;
		// 1) if piece has changed, accumulate the last arc from the last piece
		var lastInPieceDistance = last.inPieceDistance;
		if( status.pieceIndex != last.pieceIndex && last.pieceIndex > 0 ) {
			if( last.piece.angle != null ) {
				var arc = (last.pieceLength - last.inPieceDistance) / last.pieceLength;
				if( arc > 1.0 ) { arc = 1.0; }
				else if( arc < 0.0 ) { arc = 0.0; }
				angularLength += last.piece.angle * arc;
			}
			// count the current arc from the beginning
			lastInPieceDistance = 0;
		}
		// 2) add the arc of the current piece 
		if( status.piece.angle != null ) {
			var arc = (status.inPieceDistance - lastInPieceDistance) / status.pieceLength;
			if( arc > 1.0 ) { arc = 1.0; }
			else if( arc < 0.0 ) { arc = 0.0; }
			angularLength += status.piece.angle * arc;
		}
		return angularLength;
	}
	
};

function round(n) {
	return Number((n).toFixed(2));
}

module.exports = {
		Physics : Physics,
		TickStatus : TickStatus
};