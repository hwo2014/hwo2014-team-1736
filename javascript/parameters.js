function Parameters() {
}
Parameters.prototype = {
	
	// planning
	ticksForecastFactor : 9.0,		// TODO factor to be fine-tuned
	//ticksAhead : 100,				// amount of ticks to forecast
	//ticksAheadStep : 10,			// gap between ticks to forecast
	carMaxAngle : 10.0,				// maximum car angle before crashing
	carAngleError : 0.2,			// allowed error for car angle during prediction 
	maxSpeed : 10.0,				// max speed for prediction
	maxAcceleration : 2.0,			// max acceleration for prediction
	minAcceleration : -2.0,			// min acceleration for prediction
	accelerationChangeStep : 0.5,	// step factor to find step max acceleration
	maxThrottle : 1.0,				// max throttle for prediction
	minThrottle : 0.01,				// min throttle anywhere in the track
	throttleChangeFactor : 0.5,		// change factor to find step max throttle
	planningIterations : 10,		// max amount of planning iterations per piece
	
	// neural network
	//netLayers : {'drifting':[4,4],'acceleration':[4,4]},	// hidden layers
	netLayers : {'drifting':[4],'acceleration':[4]},	// hidden layers
	netLearningRate : 0.3,			// learning rate during production
	netLearningRateDebug : 0.7,		// learning rate during debug
	netTicksBetweenTrainings : 2000,// time between scheduled trainings
	netError : {'drifting':0.007,'acceleration':0.006},	// max train error
	netErrorUsage : 0.15,			// retrain if error beyond this threshold (15%)
	netIterations : 1000,			// iterations for each training

	// running mode (only activate one of these!)
	modeRace : true,
	modeSimulation : false,
	modeLearn : false,
	
	// debugging
	debug : true,
	debugGraph : true,
	debugPlot : false,
	debugProtocol : false,
	
	// plot parameters
	trackWidth : 5000,
	trackHeight : 3000,
	speedLines : false,
	//trackMetric : "drifting",
	trackMetric : "throttle",
	//trackMetric : "speed",
	//trackError : "acceleration",
	//trackError : "angularSpeed",
	//trackError : "drifting",
	metrics : [
               "throttle",
               "speed",
               "acceleration",
               "drifting",
               //"coveredAngle",
               "angularSpeed",
               //"calculationTime"
               //"angle",
               ],
               
};
module.exports = {
	Parameters : Parameters
};
