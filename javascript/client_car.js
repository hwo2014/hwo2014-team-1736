"use strict";

var util = require('util');

var helper = require('./debug_helper.js');
var physics_lib = require('./client_physics.js');
var planning_lib = require('./client_planning.js');

function Car(paper, data, track, race, debugMode) {
	this._paper = paper;
	this.debugMode = debugMode;
	this.id = data.id.name;
	this.color = data.id.color;
	// dimensions":{"length":40,"width":20,"guideFlagPosition":10}
	this.dimensions = data.dimensions;
	this.track = track;
	this.race = race;
	this.history = [];
	this.error = [];
	this.crashHistory = {};
	this.tick = -1;

	console.log("Found car '"+this.id+"'");
	
	// create layer 0: physical
	this.physics = new physics_lib.Physics(this.paper, this.track, this.race, this.dimensions);
	
	// crate layer 1: planning
	this.planning = new planning_lib.Planning(this.physics, this.track, this.race);
}
Car.prototype = {
	
	history : null,
	crashHistory : null,
	totalDistance : null,
	
	// car position
	set : function(tick, data) {
		
		// learn all properties
		this.history[tick] = this.physics.learn(tick, data, this.history[tick-1]);
		// compare to prediction to correct internal parameters
		if( this.physics.prediction != null ) {
			this.error[tick] = this.physics.feedback(this.physics.prediction, this.history[tick]);
		}
		
		// recalculate route and get desired throttle
		if( global.parameters.modeRace || global.parameters.modeSimulation ) {
			this.history[tick].throttle = this.planning.calculate(this.history[tick], tick);
		}
		
		// predict next position with desired throttle
		this.physics.predict(this.history[tick], 1, true);

	},
	
	// car has crashed
	setCrash : function(tick) {
		this.crashHistory[tick] = 1;
		if( this.history[tick] == null ) {
			this.history[tick] = {};
		}
		this.history[tick].speed = 0;
		this.history[tick].angularSpeed = 0;
		this.history[tick].acceleration = 0;
		this.history[tick].angle = 0;
		this.history[tick].drifting = 0;
		this.history[tick].crash = true;
	},
	
	// TODO car has received turbo
	setTurbo : function(tick) {
		//...
	},
	
	// saved car steering
	setThrottle : function(tick, throttle, calculationTime) {
		if( this.history[tick] != null && global.parameters.modeLearn ) {
			this.history[tick].throttle = throttle;
		}
		if( calculationTime != null && this.history[tick] != null ) {
			this.history[tick].calculationTime = calculationTime;
		}
	},
	
	// get desired throttle
	getThrottle : function(tick) {
		return this.history[tick].throttle;
	},
	
	// do we want to switch lane in next piece?
	getSwitch : function(tick) {
		var pieceId = this.history[tick].pieceIndex;
		var change = this.planning.route[pieceId].change;
		this.planning.route[pieceId].change = "changed";
		return change;
	},
	
	// dump a short message with debug information
	getDebugInfo : function(tick) {
		var last = this.history[tick];
		return tick+": t="+round(last.throttle)+", s="+round(last.speed)
			+", a="+round(last.angle)+" p="+last.coveredPieces
			+", l="+last.laneEndIndex;
	},
	
	destroy : function() {
		this.physics.destroy();
	}
	
};

function round(n) {
	return Number((n).toFixed(1));
}

module.exports = {
		Car : Car
};