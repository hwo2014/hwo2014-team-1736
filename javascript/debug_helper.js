var paper = require('paper');
var fs = require('fs');

module.exports = {
  scale : scale,
  deg2rad : deg2rad,
  saveCanvas : saveCanvas,
  DataPlotter : DataPlotter,
  Queue : Queue
};

// TODO global object with metrics
// TODO use paper.js for all point calculations, because it will be more efficient

/**
 * Create an object Queue, which is an array which
 * doesn't grow beyond the given size.
 * @param size
 */
function Queue(size) {
	this.size = size;
	this.array = [];
}
Queue.prototype = {
	size : 0,
	array : [],
	/**
	 * add a new object, removing old ones at the end
	 * of the queue if the size is higher than the given one.
	 * @param object
	 */
	add : function(object) {
		this.array.push(object);
		while( this.array.length > this.size ) {
			this.array.shift();
		}
	}
};

/**
 * Create a data plotter to file, which stores a tsv file
 * with 2 variables per point and creates a chart using gnuplot.
 */
function DataPlotter() {
}
DataPlotter.prototype = {
	name : "",
	unit : "",
	/**
	 * dump content of the queue to tsv, then call gnuplot.
	 * @param queue
	 */
	plot : function(name, unit, queue) {
		// https://github.com/richardeoin/nodejs-plotter
		var measurements = {};
		var data = {};
		for(var i=0; i<queue.array.length; i++) {
			data[queue.array[i][0]] = queue.array[i][1];
		}
		measurements[unit] = data;
		gnuplotter({
		    data: measurements,
		    time: '%H:%M:%S',
		    finish: "ls",
		    filename: __dirname + '/debug/'+name+'.png'
		});
	}
};

/**
 * Save a canvas as JPG file for debugging purposes
 * @param canvas
 * @param file
 */
function saveCanvas(canvas, file) {
	var out = fs.createWriteStream(__dirname + '/debug/'+file+'.jpg'); 
	var stream = canvas.jpegStream();
	stream.on('data', function(chunk){
	  out.write(chunk);
	});
}

var TO_RADIANS = Math.PI/180;
function deg2rad(angle) {
	return angle * TO_RADIANS;
}

/**
 * Scale a point from reference space A to space B.
 * @param point
 * @param sourceSize
 * @param targetSize
 * @returns {paper.Point}
 */
function scale (point, sourceSize, targetSize) {
	return new paper.Point(
		Math.round( point.x * targetSize.x / sourceSize.x ), 
		Math.round( point.y * targetSize.y / sourceSize.y ) );
}
