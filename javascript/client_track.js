var helper = require('./debug_helper.js');

function Track(paper) {
	this.paper = paper;
}
Track.prototype = {

	id : "",
	pieces : null,
	lanes : null,

	getPiece : function(n) {
		// modulo to reduce to the total amount of pieces
		n = n % this.pieces.length;
		return this.pieces[n];
	},
	
	decode : function(track) {

		this.id = track.id;
		this.pieces = [];
		this.lanes = track.lanes;
		
		var p = new this.paper.Point({
			x : 0,
			y : 0,
		});
		var o = new this.paper.Point({
			x : 0,
			y : 0,
			angle : track.startingPoint.angle
		});
		
		// calculate each track piece
		for( var n=0; n<track.pieces.length; n++ ) {
			var piece = new Piece(this.paper);
			piece.id = n;
			piece.length = track.pieces[n].length;
			piece.radius = track.pieces[n].radius;
			piece.angle = track.pieces[n].angle;
			
			// add point 1
			piece.point1 = p.clone();
			piece.angle1 = Math.round(o.angle);

			// set angle
			piece.angleRad = 0;

			// straight piece
			if( typeof piece.length !== 'undefined' ) {
				o.length = piece.length;
				p = p.add(o);
			}
			// curve piece
			else if( typeof piece.radius !== 'undefined' ) {
				piece.angleRad = helper.deg2rad( piece.angle );
				// http://es.wikipedia.org/wiki/Cuerda_%28geometr%C3%ADa%29
				o.length = 2 * piece.radius * Math.sin(Math.abs(piece.angleRad/2));
				o.angle += piece.angle;
				o.angle = Math.round(o.angle);
				p = p.add(o);
				piece.curvedPiece = true;
			}
			if( typeof track.pieces[n]["switch"] !== 'undefined' 
					&& track.pieces[n]["switch"] == true ) {
				piece.switchPiece = true;
			}
			
			this.pieces.push( piece );
		}
		
		// calculate each lane for each piece
		for( var n=0; n<this.pieces.length; n++ ) {
			var piece = this.pieces[n];
			var nextPiece;
			if( n >= this.pieces.length-1 ) {
				nextPiece = this.pieces[0];
			} else {
				nextPiece = this.pieces[n+1];
			}
			
			// add point 2
			piece.point2 = nextPiece.point1;
			piece.angle2 = nextPiece.angle1;

			// calculate lanes in the piece
			piece.calculate(this.lanes);
		}
		
	}

};

function Piece(paper) {
	this.paper = paper;
	this.lanes = [];
}
Piece.prototype = {

	id : 0,
	point1 : null,
	point2 : null,
	angle1 : 0,
	angle2 : 0,
	switchPiece : false,
	lanes : null,
	curvedPiece : false,
	length : null,
	radius : null,
	angle : null,
	angleRad : null,
	
	// TODO add max speed per lane per piece
	// TODO add max initial angle per lane per piece
	calculate : function(lanes) {
		
		var o = new this.paper.Point([0,0]);
		
		for(var n=0; n<lanes.length; n++) {
			var lane = {
				index : lanes[n].index,
				distanceFromCenter : lanes[n].distanceFromCenter,
				point1 : null,
				point2 : null,
				length : null,
				radius : null
			};
			// calculate point 1
			o.angle = 90+this.angle1;
			o.length = lane.distanceFromCenter;
			lane.point1 = this.point1.add(o);
			// calculate point 2
			o.angle = 90+this.angle2;
			o.length = lane.distanceFromCenter;
			lane.point2 = this.point2.add(o);
			// calculate length
			if( this.curvedPiece ) {
				// length = 2*PI*R * angle/2*PI = R * angle
				var R = this.radius - lane.distanceFromCenter;
				if( this.angle < 0 ) {
					R = this.radius + lane.distanceFromCenter;
				}
				lane.radius = R;
				lane.length = R	* Math.abs(this.angleRad);
			} else {
				lane.length = this.length;
			}
			
			this.lanes[lane.index] = lane;
		}
	},
	
	getLength : function(laneStartIndex, laneEndIndex) {
		
		var laneStart = this.lanes[laneStartIndex];
		var laneEnd = this.lanes[laneEndIndex];
		var length = laneStart.length;
		// if switching lanes, distance is different
		if( laneStartIndex != laneEndIndex ) {

			// 1) if piece is curved, length is given by logarithmic spiral
			if( this.curvedPiece ) {
				/**
				 * http://mathworld.wolfram.com/LogarithmicSpiral.html
				 * http://planetmath.org/logarithmicspiral
				 * r = a*e^(b*theta)
				 * a = R2-R1
				 * b = ln(R2/R1) / theta
				 * R2 = R1*e^(b*theta)
				 * length = (R2-R1) * sqrt(1+b^2)/b;
				 */
				var R1 = laneStart.radius;
				var R2 = laneEnd.radius;
				var theta = Math.abs(this.angleRad);
				var b = Math.log(R2/R1) / theta;
				// console.log("R2="+a*Math.exp(b*theta));
				length = (R2-R1) * Math.sqrt(1+Math.pow(b,2)) / b;
				//console.log("A) piece="+this.id+", R1="+R1+", R2="+R2+", b="+b+", length="+length);
			} 
			// 2) otherwise, we just take the straight distance between initial and end points
			else {
				length = laneStart.point1.getDistance(laneEnd.point2);
			}
		}
		return length;
		
	}
};

module.exports = {
		Track : Track
};